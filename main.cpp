#include <stdio.h>
#include "Stack.h"

int main() {
    clearLog();
    Stack_t stk1;

    //STACK_INIT( nullptr );

    STACK_INIT( &stk1 );


    stackPop( &stk1 );

    for( int i = 0; i < 20; i++ ){
        stackPush( &stk1, i );
    }
    //STACK_DUMP( &stk1, "MAIN")

    for( int i = 0; i < 10; i++ ){
        stackPop( &stk1 );
    }
    //STACK_DUMP( &stk1, "MAIN")

    stackPush( &stk1, 12 );
    stackPush( &stk1, 12 );

    stackPop( &stk1 );
    stackPop( &stk1 );
    stackPop( &stk1 );
    stackPop( &stk1 );
    //STACK_DUMP( &stk1, "MAIN")




    stackPush( &stk1, 1 );
    stackPush( &stk1, 2 );
    stackPush( &stk1, 3 );
    stackPush( &stk1, 4 );
    stackPush( &stk1, 5 );
    stackPush( &stk1, 6 );
    stackPush( &stk1, 1 );
    stackPush( &stk1, 2 );
    stackPush( &stk1, 3 );

    //stk1.hash = 0;
    //*((char*)(&stk1)+50) = '0';

    stackPush( &stk1, 4 );
    stackPush( &stk1, 5 );
    stackPush( &stk1, 6 );
    //STACK_DUMP( &stk1, "MAIN")

    return 0;
}