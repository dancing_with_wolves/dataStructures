//
// Created by dimoha_zadira on 23.09.2019.
//

#ifndef DATASTRUCTURE_STACK_H
#define DATASTRUCTURE_STACK_H


#include <type_traits>

#define DEBUG 0

#ifdef DEBUG
#define ASSERT_OK( cond ){                                                                                              \
        if( !(cond) ) {                                                                                                 \
            FILE *out = fopen ( "stackLog.txt", "at");                                                                  \
            fprintf( out, "ASSERTION FAILED: %s, file %s, line %d, func %s\n", #cond, __FILE__, __LINE__, __func__);    \
            fclose( out );                                                                                              \
            abort();                                                                                                    \
        }                                                                                                               \
    }
#else
#define ASSERT_OK( cond ) cond;
#endif

#define STACK_DUMP( stk, reason ) tellMeEverythingYouKnow( #stk, stk, reason, __func__, __LINE__, __FILE__ );

#define STACK_INIT( stk ) stackInit( stk, #stk )


typedef int Elem_t;

enum StackError{
    OK, NEGATIVE_SIZE, INACCESSIBLE_DATA, VOLUME_OVERFLOW, WAS__NULL_POINTER, OUT_OF_RAM, EXTERNAL_INTRUSION
};

/*!
 *
 */
struct Stack_t{
    unsigned long long canaryBegin;
    StackError error;
    Elem_t *data;
    int size;
    int maxStackSize;
    char *stackName;
    unsigned long long hash;
    unsigned long long canaryEnd;
};

/*!
 * Увеличивает размер стека в два раза
 * @param this_ Стек
 * @return Код ошибки
 */
StackError moreVolume( Stack_t *this_ );

/*!
 * Уменьшает размер стека в два раза
 * @param this_ Стек
 * @return Код ошибки
 */
StackError lessVolume( Stack_t *this_ );

/*!
 * Чистит лог
 */
void clearLog();

/*!
 * Инициализирует пустой стек
 * @param this_ Указатель на стек
 * @param err Код ошибки
 * @param stackName Имя стека (не обязательно указывать)
 * @return Возвращает код ошибки
 */
int stackInit( Stack_t *this_, char *stackName = "I_Am_Stack", StackError err = OK);

/*!
 * Возвращает элемент, лежащий в стеке последним
 * @param this_ Указатель не стек
 * @return Элемент, лежащий в стеке последним
 */
Elem_t stackPop( Stack_t *this_ );

/*!
 * Кладёт в конец стека элемент value
 * @param this_ Указатель не стек
 * @param value Elem_t элемент, который будет положен
 * @return Возвращает код ошибки
 */
int stackPush( Stack_t *this_, Elem_t value );

/*!
 * Держит в курсе
 * @param name Имя стека в функции, из которого вызвана tellMeEverything
 * @param this_ Указатель не стек
 * @param reason С какой целью интересуетесь?
 * @param func Название функции, из которой вызвана
 * @param line Номер строки, и которой вызвана
 * @param file Файл, из которого вызвана
 * @return Код ошибки
 */
int tellMeEverythingYouKnow( char *name, Stack_t *this_, char *reason, const char *func, int line, char *file );

/*!
 * Саня, ты в порядке?
 * @param this_ Указатель на проверяемый стек
 * @return Возвращает код ошибки
 */
int stackOK( Stack_t *this_ );

#endif //DATASTRUCTURE_STACK_H
