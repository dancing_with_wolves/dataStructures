//
// Created by dimoha_zadira on 23.09.2019.
//

#include <string.h>
#include <stdio.h>
#include "Stack.h"
#include "assert.h"
#include <typeinfo>
#include <malloc.h>
#include <climits>
#include <cstdlib>

const char separation[] = "\n------------------------------------------------------------------------------------\n";
const char logName[] = "stackLog.log";
const Elem_t POISON_ELEM = INT_MAX;
int hysteresis = 1;
const int hashSimpleMod = 914579;
const int qtyDivider = 1721;

const char* MyErrorMessages[]{
        "OK", "NEGATIVE_SIZE", "INACCESSIBLE_DATA", "VOLUME_OVERFLOW", "WAS__NULL_POINTER", "OUT_OF_RAM", "EXTERNAL_INTRUSION"
};
/*!
 * Записывает сообщение в лог
 * @param msg Сообщение
 */
void msgToLog( char *msg )
{
    FILE *log = fopen ( logName, "at" );
    fprintf( log, "%s%s", msg, separation );
    fclose( log );
}
/*!
 * Считает unsigned long long hash заданного стека this_
 * @param this_ Указатель на стек
 * @return Возвращает посчитанный хеш
 */
unsigned long long countHash( Stack_t *this_ )
{
    unsigned long long ret = 0;
    int i = 0;

    char *cur = (char*) this_;
    char *end = (char*) ( (&this_-> canaryEnd) + sizeof( this_-> canaryEnd ) );

    char *skipPointer = (char*) (&this_-> hash);
    int skipBytes = sizeof(this_-> hash);

    while( cur != end ){
        if( cur == skipPointer ) cur += skipBytes;
        ret += ( ( i % qtyDivider) + 1 ) * ( *cur );
        ret = ret % hashSimpleMod;
        cur++;
    }
    return ret;
}

/*!
 * Проверяет состояние стека
 * @param this_ Стек
 * @return Состояние стека: VOLUME_OVERFLOW, NEGATIVE_SIZE, INACCESSIBLE_DATA, OK
 */
int stackOK( Stack_t *this_ )
{
    if( !this_ ){
        return WAS__NULL_POINTER;
    }
    if( this_-> size > this_-> maxStackSize ){
        this_-> error = VOLUME_OVERFLOW;
        STACK_DUMP( this_, "VOLUME_OVERFLOW")
        return VOLUME_OVERFLOW;
    }

    if( this_-> size < 0 ){
        this_-> error = NEGATIVE_SIZE;
        STACK_DUMP( this_, "NEGATIVE_SIZE")
        return NEGATIVE_SIZE;
    }
    if( !this_-> data ){
        this_->error = INACCESSIBLE_DATA;
        STACK_DUMP( this_, "INACCESSIBLE_DATA")
        return INACCESSIBLE_DATA;
    }
    if( this_-> canaryEnd != (int)('c'+'a'+'n'+'a'+'r'+'y'+'e'+'n'+'d') || ( this_-> canaryBegin != (int)('c'+'a'+'n'+'a'+'r'+'y'+'b'+'e'+'g'+'i'+'n') ) ){
        msgToLog( "Кто-то ломает меня полностью! *БЕЗУМНЫЙ КРИК УМИРАЮЩЕЙ КАНАРЕЙКИ*" );
        this_-> error = EXTERNAL_INTRUSION;
        STACK_DUMP( this_, "EXTERNAL_INTRUSION")
        return EXTERNAL_INTRUSION;
    }
    if( this_-> hash != countHash( this_ ) ){
        msgToLog( "Кто-то ломает меня полностью! *ХЕШ-ФУНКЦИЯ ОБЛИВАЕТСЯ ХОЛОДНЫМ ПОТОМ*" );
        this_-> error = EXTERNAL_INTRUSION;
        STACK_DUMP( this_, "EXTERNAL_INTRUSION!")
        return EXTERNAL_INTRUSION;
    }
    this_-> error = OK;
    return OK;
}


int stackInit( Stack_t *this_, char *stackName, StackError err )
{
    ASSERT_OK( this_ );

    if( this_ ) {
        this_-> maxStackSize = 2;

        this_-> data = (Elem_t *) calloc( this_-> maxStackSize, sizeof(Elem_t) );
        for ( int i = 0; i < this_-> maxStackSize; i++ ){
            this_-> data[i] = POISON_ELEM;
        }
        this_-> canaryBegin = (int)('c'+'a'+'n'+'a'+'r'+'y'+'b'+'e'+'g'+'i'+'n');
        this_-> canaryEnd = (int)('c'+'a'+'n'+'a'+'r'+'y'+'e'+'n'+'d');

        this_-> error = err;
        this_-> size = 0;
        this_-> stackName = strdup( stackName );
        this_-> hash = countHash( this_ );
    } else {
        msgToLog( "В stackInit передан null!\n" );
        return WAS__NULL_POINTER;
    }
    return 1;
}



StackError moreVolume( Stack_t *this_ )
{
    msgToLog( "Выделяю стеку в два раза больше места... ");

    Elem_t *tmp = (Elem_t*) realloc( this_-> data, 2 * this_-> maxStackSize * sizeof( Elem_t ) );
    if( tmp == nullptr ) {
        msgToLog( "REALLOC НЕ МОЖЕТ ВЫДЕЛИТЬ ПАМЯТЬ" );
        this_-> hash = countHash( this_ );
        return OUT_OF_RAM;
    }
    if( tmp != this_-> data ) {
        msgToLog( "Память реаллоцирована..." );
    }
    this_-> data = tmp;
    this_-> maxStackSize *= 2;

    for (  int i = this_-> maxStackSize/2; i < this_-> maxStackSize; i++ ){
        this_-> data[i] = POISON_ELEM;
    }

    this_-> hash = countHash( this_ );

    ASSERT_OK( stackOK( this_ ) == OK );
    return OK;
}



StackError lessVolume( Stack_t *this_ )
{
    msgToLog( "Уменьшаю размер стека в два раза... ");
    ASSERT_OK( stackOK( this_ ) == OK );

    this_-> maxStackSize /= 2;
    this_-> data = (Elem_t *) realloc( this_-> data, this_-> maxStackSize * sizeof( Elem_t ) );

    this_-> hash = countHash( this_ );
    ASSERT_OK( stackOK( this_ ) == OK );

    return OK;
}

Elem_t stackPop( Stack_t *this_ )
{
    ASSERT_OK( stackOK( this_ ) == OK );

    hysteresis = this_-> maxStackSize / 8 + 1;
    if( this_-> size <= 0 ) {
        this_-> error = NEGATIVE_SIZE;
        this_-> hash = countHash( this_ );

        msgToLog( "Ай! Вырубай! Попытка взять элемент из пустого стека" );
        STACK_DUMP( this_, "NEGATIVE_SIZE")


        return POISON_ELEM;
    }
    if( this_-> size < ( ( this_-> maxStackSize / 2 ) - hysteresis ) ){
        lessVolume( this_ );
    }
    if( this_-> size > 0 ) {
        this_-> size--;
        Elem_t ret = this_->data[this_->size];
        this_-> data[this_->size] = POISON_ELEM;

        this_-> hash = countHash( this_ );

        return ret;
    }
}



int stackPush( Stack_t *this_, Elem_t value )
{
    ASSERT_OK( stackOK( this_ ) == OK );
    if( this_-> size >= this_-> maxStackSize ) {
        msgToLog( "Ай! Вырубай! Попытка закинуть элемент в полный стек" );
        this_-> error = moreVolume( this_ );
        //return OUT_OF_RAM;
    }

    if( this_-> size < this_-> maxStackSize ){
        this_-> data[ this_-> size++ ] = value;
    }

    this_-> hash = countHash( this_ );
    ASSERT_OK( stackOK( this_ ) == OK );

    return OK;
}



/*!
 * Очищает файл лога
 */
void clearLog()
{
    FILE *out = fopen ( logName, "w" );
    fclose( out );
}

int tellMeEverythingYouKnow( char *name, Stack_t *this_, char *reason, const char *func, int line, char *file )
{
    FILE *out = fopen ( logName, "at" );
    fseek( out, 0, SEEK_END );

    fprintf( out, "STACK %s called %s; name in function = %s\n", typeid (this_).name(), this_-> stackName, name );
    fprintf( out, "%s from function <%s> from file <%s>, line <%d>:\n", reason, func, file, line );

    int size = this_-> size;
    fprintf( out, "stack -> size = <%d>\n", size );
    fprintf( out, "stack -> maxStackSize = <%d>\n", this_-> maxStackSize );
    fprintf( out, "stack -> data = [%p]\n", this_ -> data );

    for ( int i = 0; i < this_-> maxStackSize; i++ ){
        if( i < size ) fprintf( out, "\t*" ); else fprintf( out, "\t " );
        fprintf( out, "data[%d] = <%d>", i, this_-> data[i] );
        if ( this_-> data[i] == POISON_ELEM ) fprintf ( out, " [POISON]\n" ); else fprintf ( out, "\n" );
    }

    fprintf( out, "err = %d: %s\n", this_-> error, MyErrorMessages [this_ -> error ]);

    unsigned long long realHash = countHash( this_ );
    fprintf( out, "HASH: %llu; SHOULD BE: %llu", this_->hash, realHash);
    if( realHash == this_-> hash ) fprintf( out, " Hash correct\n"); else fprintf( out, " HASH INCORRECT!\n");
    fprintf( out, "%s", separation );

    fclose( out );
    return 0;
}









